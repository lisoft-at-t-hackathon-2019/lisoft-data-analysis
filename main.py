from flask import Flask, request, jsonify

app = Flask(__name__)


@app.route('/prediction', methods=['GET'])
def prediction():
    # V tuhle chvili zavolej fuknci, která vybere data z databáze od Muffa (request na API)
    # Potom zanalizuj data a dej ten objekt do proměnný data (Analyza)
    data = {"hello": "world!"} # vymen objekt za zanalizovaný data
    return data, 200


if __name__ == "__main__":
    app.run(debug=True, port=8000)
