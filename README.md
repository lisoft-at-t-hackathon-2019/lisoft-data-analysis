# Data analysis service
Python service for analysing and predicting future product management.

# Routes

### /prediction
Route is waiting for GET requests with no specific headers.

#### Response:
	{
		[
			{
				"id": device name,
				"prediction": [
				    {
				        "datetime": predicted time,
					    "product_key": id of product,
					    "amount": predicted number of products
				    }
				]
			}
		]
	}

# Author
Radek Kucera & Marek Dohnal | **radakuceru@gmail.com**, **marek.dohnal@pslib.cz**
