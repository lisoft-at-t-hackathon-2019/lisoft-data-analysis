import json
import pandas as pd
import requests
import matplotlib.pyplot as plt
from statsmodels.tsa.api import ExponentialSmoothing
from sklearn.metrics import mean_squared_error

#test data
url = "https://newagent-a153e.firebaseio.com/users/1/238asdb128bascd8dd890/products/1.json"
response = requests.get(url)
data_json = json.loads(response.text)
print(data_json)
print(data_json["historicalData"])

df = pd.read_json(json.dumps(data_json["historicalData"]), orient="index")
df.columns = ["Amount"]
df.index = pd.to_datetime(df.index)

df["Amount"].plot()
plt.show()


#expected json format:
#json:{"storages" : [{"eid" : "x", data = { "datetime" : { "product_key" : "x", "amount": "x" }}]}

#generator yielding storage data entries
def load_data_from_json(data_json):
    data_json_dummy = json.loads(data_json)
    for storage in (data_json_dummy["storages"]):
        storage_ID = storage["eid"]
        storage_data = storage["data"]
        #print(storage_ID)
        #print(storage_data)
        yield storage_ID, storage_data

def predict_data(storage_data_json):
    #foreach storage data entry
    #process, prepare and predict
    for i in range(len(storage_data_json)):
        #print(storage_data_json[i])
        #storage_data_df = pd.read_json(json.dumps(storage_data_json[i]), orient="index")
        for u_p_key in (storage_data_df.product_key.unique()):
            product_df = storage_data_df[storage_data_df["product_key"] == u_p_key]
            #product_df = product_df.set_index("datetime")
            #print(product_df.head())
            
            #temp.py for explanation
            model_fit = ExponentialSmoothing(product_df, seasonal_periods=6, trend="add",
                                     seasonal="add").fit(use_boxcox=True)
            predicted_data = model_fit.forecast(168)
            predict_data_json = predicted_data.to_json(orient="index")
            
            return predicted_data_json

def main():
    predicted_data_json = { "storages": []}
    for storage_ID, storage_data in load_data_from_json(response.text):
        storage_data_json = { "eid" : storage_ID, "predicted_data" : None}
        predicted_data = predict_data(storage_data)
        print(predicted_data)
        storage_data_json["predicted_data"] = predicted_data
        predicted_data_json["storages"].append(storage_data_json)

    #send data logic
    #appears to be missing :/
    
    print(json.loads(json.dumps(predicted_data_json, indent=2)))

#main()
