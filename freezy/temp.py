import json
import pandas as pd
import numpy as np
from datetime import datetime
from statsmodels.tsa.api import ExponentialSmoothing
import requests
from matplotlib import pyplot as plt

#random data each 10mins for a week
date_range = pd.date_range(start="10/01/2019",
                           end="10/08/2019",
                           freq="10min")

#data loaded into pandas.dataframe for better manipulation
df = pd.DataFrame(date_range, columns=["datetime"])
df["product_key"] = np.random.randint(1, 100, size=(len(date_range)))
df["amount"] = np.random.randint(1, 100, size=(len(date_range)))

df["datetime"] = pd.to_datetime(df["datetime"])
df = df.set_index("datetime")

# df["product_key"].plot()

#data fitted into ETS model for time-series prediction
#seasonal_periods == frequency
model_fit1 = ExponentialSmoothing(df["amount"], seasonal_periods=6, trend="add",
                                  seasonal="add").fit(use_boxcox=True)

#model_fit2 = ExponentialSmoothing(df["amount"], seasonal_periods=24, trend="add",
#                                 seasonal="mul", damped=True).fit(use_boxcox=True)


forecast1 = model_fit1.forecast(168)
#forecast2 = model_fit2.forecast(24)
forecast1.plot()
#forecast2.plot()
plt.show()


#plotting time-series data to analyze Trend, Seasonality etc...
#decomposition = sm.tsa.seasonal_decompose(df, model='additive')
#fig = decomposition.plot()
#plt.show()


json_df = df.to_json(json, orient="index")

dummy_data_json_string = { "storages": [{"id" : "mrazak", "data": json_df},
                                        {"id" : "chladnicka", "data": json_df},
                                        {"id" : "spizirna", "data": json_df}
]}
dummy_data_json = json.dumps(dummy_data_json_string)

#print(dummy_data_json_string["storages"])

#print(json.loads(json.dumps(dummy_data_json_string)))


def load_data_from_json(data_json):
    data_json_dummy = json.loads(data_json)
    for storage in (data_json_dummy["storages"]):
        storage_ID = storage["eid"]
        storage_data = storage["data"]
        print(storage_ID)
        print(storage_data)
        yield storage_ID, storage_data


        
def predict_data(storage_data_json):
    #print(storage_data_json)
    #storage_data_df = pd.read_json(storage_data_json, orient="index")
    #print(storage_data_df.head())
    #storage_data_df["datetime"] = pd.to_datetime(storage_data_df["datetime"])
    #storage_data_df = storage_data_df.set_index("datetime")

    for i in range(len(storage_data_json)):
        print(storage_data_json[i])
        #storage_data_df = pd.read_json(json.dumps(storage_data_json[i]), orient="index")
        for u_p_key in (storage_data_df.product_key.unique()):
            product_df = storage_data_df[storage_data_df["product_key"] == u_p_key]
            #product_df = product_df.set_index("datetime")
            print(product_df.head())
            product_df_json = product_df.to_json(orient="index")
            print(product_df.head())        

            return product_df_json
        
        #print(product_df_json)
    #predicted_data = {"datetime": None, "product_key" : None, "amount": None }
    #yield predicted_data

def main():
    response = requests.get("http://10.10.10.77:3002/api/records")
    predicted_data_json = { "storages": []}
    for storage_ID, storage_data in load_data_from_json(response.text):
        #print(storage_ID)
        #print(storage_data)
        storage_data_json = { "eid" : storage_ID, "predicted_data" : None}
        predicted_data = predict_data(storage_data)
        print(predicted_data)
        storage_data_json["predicted_data"] = predicted_data
        #print(storage_data_json)
        predicted_data_json["storages"].append(storage_data_json)
        
    print(json.loads(json.dumps(predicted_data_json, indent=2)))

#main()


# predicted_data_json_string = df.to_json(orient="records", lines=True)


# json_string_to_send = {
#     "storages" : [
#         {
#             "storage_id": "mrazak",
#             "data": predicted_data_json_string
#         },
#         {
#             "storage_id": "chladak",
#             "data": predicted_data_json_string
#         },
#         {
#             "storage_id": "spizirna",
#             "data": predicted_data_json_string
#         }
#     ]        
# }

# def 




# def main():

#     
#     
            
#     
      #data = data_controller.load_predicted_data_json(predicted_data_json)
# #print(json.loads(json.dumps(obj_json)))


#json_loaded = json.loads(json.dumps(obj_json))
#data_loaded = json_loaded["data"]

#data_loaded_df = pd.read_json(data_loaded, orient="records", lines=True)
#data_loaded_df["datetime"] = pd.to_datetime(df['datetime'])
#data_loaded_df = data_loaded_df.set_index("datetime")
#print(data_loaded_df.head())


#print(data_json)

#print(df.head())

